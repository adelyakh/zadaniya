﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Console1
{
    class Program
    {
        static void delimost(int a,int b)
        {
            for (int i = a; i <= b; i++)
            {
                if (i % 2 == 0)
                    Console.WriteLine(i + " делится на 2");
                if (i % 3 == 0)
                    Console.WriteLine(i + " делится на 3");

            }
        }
        
        public static int[] sort(int[] mas)
        {
            if (mas.Length == 1)
                return mas;
            int mid_point = mas.Length / 2;
            return merge(sort(mas.Take(mid_point).ToArray()), sort(mas.Skip(mid_point).ToArray()));
        }
        public static int[] merge(int[] mass1, int[] mass2)
        {
            int a = 0, b = 0;
            int[] merged = new int[mass1.Length + mass2.Length];
            for (int i = 0; i < mass1.Length + mass2.Length; i++)
            {
                if (b < mass2.Length && a < mass1.Length)
                    if (mass1[a] > mass2[b] && b < mass2.Length)
                        merged[i] = mass2[b++];
                    else
                        merged[i] = mass1[a++];
                else
                    if (b < mass2.Length)
                    merged[i] = mass2[b++];
                else
                    merged[i] = mass1[a++];
            }
            return merged;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите диапазон чисел. От ");

            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("до ");
            int b = Convert.ToInt32(Console.ReadLine());
            delimost(a,b);
            Console.WriteLine("\n массив #1 из 10 элементов: ");
            int [] mas = new int [10];
            Random r = new Random();
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = r.Next(0, 200);
                Console.Write(mas[i] + " ");
            }
            Console.WriteLine("\n Отсортированный массив #1: ");
            mas = sort(mas);
            Console.WriteLine(string.Join(" ", sort(mas)));
            Console.ReadKey();
            Console.WriteLine("\n массив #2 из 10 элементов: ");
            int[] mas1 = new int[10];
            for (int i = 0; i < mas1.Length; i++)
            {
                mas1[i] = r.Next(0, 200);
                Console.Write(mas1[i] + " ");
            }
            Console.WriteLine("\n Массив #1 + массив #2: ");
            int[] mas2 = new int[10];
            for (int i = 0; i < mas2.Length; i++)
            {
                mas2[i] = mas[0 + i] + mas1[0+i];
                Console.Write(mas2[i] + " ");
            }
        }
    }
}
